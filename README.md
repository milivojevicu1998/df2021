# Forenzika mrežnog saobraćaja iz PCAPNG datoteka

Dokdiranje saobraćaja učitanog iz PCAPNG datoteka i filtriranje dekodiranih paketa.

## Pokretanje

Prvo potrebno je instalirati python-pcapng biblioteku:

```bash
pip install python-pcapng
```

Zatim se program može pokrenuti pozivanjem `python` interpretera nad direktorijumom celog projekta.
U korenu repositorijuma:

```bash
python .
```

## Korišćenje

TODO
