#!python


import re
import sys
import base64
import urllib.parse

from pcapng import FileScanner
from pcapng.blocks import EnhancedPacket

from src.utils import *
from src.eth2 import EthernetII
from src.ipv4 import IPv4
from src.tcp import TCP
from src.http import HTTP


F_PORTS = [80, 8000, 8008, 8080]
F_IPS = []
F_KEYWORDS = [
    'month',
    'password',
    'pass',
    'pas',
    'pwd',
    'psw',
]


def decode_enhanced_packet(packet_name: str, packet: EnhancedPacket):
    data = packet.packet_data

    p_eth2 = EthernetII(data)
    data = p_eth2.rest

    if p_eth2.type_str == 'IPv4':
        p_ipv4 = IPv4(data)
        data = p_ipv4.rest

        # Filter by ip
        if len(F_IPS) != 0 and (p_ipv4.addr_dst not in F_IPS and p_ipv4.addr_src not in F_IPS):
            print('%s %sYYY%s Packet filtered out by IP' % (packet_name, COLOR_BLUE, STYLE_RESET))
            return
    else:
        print('%s %s!!!%s Packet filtered, not IPv4' % (packet_name, COLOR_RED, STYLE_RESET))
        return

    if p_ipv4.protocol_str == 'TCP':
        p_tcp = TCP(data)
        data = p_tcp.rest

        # Filter by port
        if len(F_PORTS) != 0 and (p_tcp.port_dst not in F_PORTS and p_tcp.port_src not in F_PORTS):
            print('%s %sYYY%s Packet filtered out by PORT' % (packet_name, COLOR_BLUE, STYLE_RESET))
            return
    else:
        print('%s %s!!!%s Packet filtered, not TCP' % (packet_name, COLOR_RED, STYLE_RESET))
        return

    p_http = HTTP(data, F_KEYWORDS)
    if p_http is None:
        print('%s %s!!!%s Packet filtered, not HTTP' % (packet_name, COLOR_RED, STYLE_RESET))
        return

    # Log
    print()
    print(packet_name)
    print(p_eth2)
    print(p_ipv4)
    print(p_tcp)
    print(p_http)
    print()


def process(scanner: FileScanner):
    for block in scanner:
        packet_name = STYLE_BOLD + STYLE_UNDERLINE + block.__class__.__name__ + STYLE_RESET
        if isinstance(block, EnhancedPacket):
            decode_enhanced_packet(packet_name, block)
        else:
            print('%s %s!!!%s Can\'t parse this packet type, skipping' % (packet_name, COLOR_RED, STYLE_RESET))


def main():
    if len(sys.argv) <= 1:
        print('usage: %s FILE [FILE ...]' % sys.argv[0])
        sys.exit(1)

    for filename in sys.argv[1:]:
        print(' ***', filename, end='\n\n')
        file = sys.argv[1]

        scanner = None
        with open(file, 'rb') as file:
            scanner = FileScanner(file)
            process(scanner)


main()
