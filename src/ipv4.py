from .utils import STYLE_BOLD, STYLE_RESET

class IPv4:
    """
    IPv4 header class.

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |Version|  IHL  |Type of Service|          Total Length         |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |         Identification        |Flags|      Fragment Offset    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  Time to Live |    Protocol   |         Header Checksum       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                       Source Address                          |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Destination Address                        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Options                    |    Padding    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    In class:
    * length    <-  4b @  0B+4b
    * id        <-  2B @  4B
    * flags     <-  3b @  6B
    * offset    <- 13b @  6B+3b
    * protocol  <-  1B @  9B (TCP = 0x06)
    * addr_src  <-  4B @ 12B
    * addr_dst  <-  4B @ 16B
    """

    PROTOCOLS = {
        6: 'TCP'
    }

    FLAGS = [
        '?',  # Reserved
        'D',  # Don't fragment
        'M',  # More fragments
    ]

    def __init__(self, data: bytes):
        # Length is in 4B words
        self.length: int = (data[0] & 63) * 4

        self.data: bytes = data[:self.length]
        self.rest: bytes = data[self.length:]

        self.id: int = int.from_bytes(data[4:6], 'big')

        tmp_flags: str = bin(data[6])[2:-5]
        tmp_flags = (len(self.FLAGS) - len(tmp_flags)) * '0' + tmp_flags
        self.flags: str = ''
        for i in range(len(self.FLAGS)):
            self.flags += self.FLAGS[i] if tmp_flags[i] == '1' else '.'

        self.offset = data[7] + data[6] & int('00011111', 2) * pow(2, 8)
        self.protocol: int = data[9]
        self.addr_src: bytes = data[12:16]
        self.addr_dst: bytes = data[16:20]

    @property
    def addr_src_str(self) -> str:
        return '.'.join([str(byte) for byte in self.addr_src])

    @property
    def addr_dst_str(self) -> str:
        return '.'.join([str(byte) for byte in self.addr_dst])

    @property
    def protocol_str(self) -> str:
        if self.protocol in self.PROTOCOLS:
            return self.PROTOCOLS[self.protocol]
        else:
            return 'Unkown (%02x)' % self.protocol

    def __str__(self):
        return ('%sIPv4%s\t\t%dB\tid=%4x, %s %5d, %s -> %s, %s' % (
            STYLE_BOLD,
            STYLE_RESET,
            self.length,
            self.id,
            self.flags,
            self.offset,
            self.addr_src_str,
            self.addr_dst_str,
            self.protocol_str,
        ))
