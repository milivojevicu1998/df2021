from .utils import STYLE_BOLD, STYLE_RESET

class EthernetII:
    """
    EthernetII header class. Header is fixed length of 14 bytes.

    In class:
    * addr_dst  <-  6B @  0B
    * addr_src  <-  6B @  6B
    * type      <-  2B @ 12B (IPv4 = 0x0800)
    """

    TYPES = {
        b'\x08\x00': 'IPv4'
    }

    def __init__(self, data: bytes):
        # Fixed length
        self.length = 14

        self.data: bytes = data[:self.length]
        self.rest: bytes = data[self.length:]

        self.addr_dst: bytes = self.data[0:6]
        self.addr_src: bytes = self.data[6:12]
        self.type: bytes = self.data[12:14]

    @property
    def addr_src_str(self) -> str:
        return ':'.join([hex(pair)[2:] for pair in self.addr_src])

    @property
    def addr_dst_str(self) -> str:
        return ':'.join([hex(pair)[2:] for pair in self.addr_dst])

    @property
    def type_str(self) -> str:
        if self.type in self.TYPES:
            return self.TYPES[self.type]
        else:
            return 'Unkown (%s)' % ('0x' + ''.join(['%02x' % byte for byte in self.type]))

    def __str__(self):
        return ('%sEthernetII%s\t%dB\t%s -> %s, %s' % (
            STYLE_BOLD,
            STYLE_RESET,
            self.length,
            self.addr_src_str,
            self.addr_dst_str,
            self.type_str,
        ))
