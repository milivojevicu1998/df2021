import re
import base64
import urllib.parse
from typing import List

from .utils import *


class HTTP:
    REQUEST = 0
    RESPONSE = 1

    def __new__(cls, data: str, keywords: List[str]):
        try:
            decoded = bytes.decode(data)

            obj = object.__new__(cls)
            obj.data = decoded

            re_request = re.search('[A-Z]{3,4} \\/[\\w\\.?&=]* HTTP\\/\\d(\\.\\d)?', decoded)
            if re_request is not None and re_request.start() == 0:
                obj.type = cls.REQUEST
                obj.find_auth()
                obj.find_kws(keywords)
                return obj

            re_response = re.search('HTTP\\/\\d(\\.\\d)? (\\d){3} [\\w ]+', decoded)
            if re_response is not None and re_response.start() == 0:
                obj.type = cls.RESPONSE
                return obj

            return None
        except UnicodeDecodeError:
            return None

    def find_auth(self):
        re_auth = re.search('Authorization: Basic [a-zA-Z0-9+/=]*', self.data)

        if re_auth is not None:
            self.data = \
                self.data[:re_auth.end()] + \
                COLOR_RED + \
                '  -> ' + \
                bytes.decode(base64.b64decode(self.data[re_auth.start() + 21:re_auth.end()])) + \
                STYLE_RESET + \
                self.data[re_auth.end():]

    def find_kws(self, keywords: List[str]):
        pattern = re.compile('[\\n?&]([\\w%]+=[\\w%]+)')
        keywords = re.compile('(' + '|'.join([('(' + word + ')') for word in keywords]) + ')')

        pos = 0
        result = pattern.search(self.data, pos)
        while result is not None:
            match = urllib.parse.unquote(self.data[result.start() + 1:result.end()])
            keyword_match = keywords.search(match.split('=')[0])
            if keyword_match is not None:
                match = COLOR_RED + match + STYLE_RESET
            self.data = \
                self.data[:result.start() + 1] + \
                match + \
                self.data[result.end():]
            pos = result.end() + (len(match) - (result.end() - result.start() + 1))
            result = pattern.search(self.data, pos)

    def __str__(self):
        return ('%sHTTP%s\t\t%dB\n--- START ---\n%s\n---  END  ---' % (
            STYLE_BOLD,
            STYLE_RESET,
            len(self.data),
            self.data,
        ))
