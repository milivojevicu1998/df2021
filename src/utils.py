# Resets style and color
STYLE_RESET = '\x1b[0m'

# Styles
STYLE_BOLD = '\x1b[1m'
STYLE_FAINT = '\x1b[2m'
STYLE_UNDERLINE = '\x1b[4m'
STYLE_BLINK = '\x1b[5m'

# Foreground colors
COLOR_RED = '\x1b[31;1m'
COLOR_BLUE = '\x1b[36;1m'

# Other
COLOR_BLOCK = '\x1b[30;41m'  # red background, black foreground
