from .utils import STYLE_BOLD, STYLE_RESET

class TCP:
    """
    TCP header class.

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |          Source Port          |       Destination Port        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                        Sequence Number                        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Acknowledgment Number                      |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | Offset| Rsrv. |N|E|U|A|P|R|S|F|            Window             |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |           Checksum            |         Urgent Pointer        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Options                    |    Padding    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                         Data ...                              |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    In class:
    * port_src  <-  2B @  0B
    * port_dst  <-  2B @  2B
    * seq_num   <-  4B @  4B
    * length    <-  4b @ 12B
    * flags     <-  9b @ 12B+7b
    """

    FLAGS = [
        '?',  # Reserved
        '?',  # Reserved
        '?',  # Reserved
        'N',  # Nonce
        'C',  # CWR
        'E',  # Echo
        'U',  # Urgent
        'A',  # Acknowledgment
        'P',  # Push
        'R',  # Reset
        'S',  # Syn
        'F',  # Fin
    ]

    def __init__(self, data: bytes):
        # Length in 4B words
        self.length: int = ((data[12] & int('11110000', 2)) >> 4) * 4

        self.data: bytes = data[:self.length]
        self.rest: bytes = data[self.length:]

        self.port_src: int = int.from_bytes(data[0:2], 'big')
        self.port_dst: int = int.from_bytes(data[2:4], 'big')

        self.seq_num: int = int.from_bytes(data[4:8], 'big')

        tmp_flags: str = bin(data[13] + (data[12] & int('00001111', 2)) * pow(2, 8))[2:]
        tmp_flags = (len(self.FLAGS) - len(tmp_flags)) * '0' + tmp_flags
        self.flags: str = ''
        for i in range(len(self.FLAGS)):
            self.flags += self.FLAGS[i] if tmp_flags[i] == '1' else '.'

    def __str__(self):
        return ('%sTCP%s\t\t%dB\t%d -> %d, %d, %s' % (
            STYLE_BOLD,
            STYLE_RESET,
            self.length,
            self.port_src,
            self.port_dst,
            self.seq_num,
            self.flags,
        ))
